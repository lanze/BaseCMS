package com.cms.entity.weixin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.cms.common.hibernate.BaseEntity;

/** 
 * @ClassName: MsgNews 
 * @Description: TODO(图文消息) 
 * @author zhangp 
 * @date 2016年5月9日 下午1:58:17 
 *  
 */
@SuppressWarnings("serial")
@Entity(name="t_weixin_msgnews")
public class MsgNews extends BaseEntity {
	@Column
	private String wxAccount;//微信账号
	@Column
	private String title;// 标题
	@Transient
	private String title_like;
	@Column
	private String author;// 作者
	@Column
	private String brief;// 简介
	@Column
	private String description;// 描述
	@Column
	private String picpath;// 封面图片
	@Column
	private String picdir;// 封面图片绝对目录
	@Column
	private Integer showpic;// 是否显示图片
	@Column
	private String url;// 图文消息原文链接
	@Column
	private String fromurl;// 外部链接

	@Column
	private String msgtype;// 消息类型;
	@Column
	private String inputcode;// 关注者发送的消息
	@Transient
	private String inputcode_like;
	@Column
	private String rule;// 规则，目前是 “相等”
	@Column
	private Integer enable;// 是否可用
	@Column
	private Integer readcount;// 消息阅读数
	@Column
	private Integer favourcount;// 消息点赞数


	public String getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(String msgtype) {
		this.msgtype = msgtype;
	}

	public String getInputcode() {
		return inputcode;
	}

	public void setInputcode(String inputcode) {
		this.inputcode = inputcode;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public Integer getEnable() {
		return enable;
	}

	public void setEnable(Integer enable) {
		this.enable = enable;
	}

	public Integer getReadcount() {
		return readcount;
	}

	public void setReadcount(Integer readcount) {
		this.readcount = readcount;
	}

	public Integer getFavourcount() {
		return favourcount;
	}

	public void setFavourcount(Integer favourcount) {
		this.favourcount = favourcount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicpath() {
		return picpath;
	}

	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}

	public String getPicdir() {
		return picdir;
	}

	public void setPicdir(String picdir) {
		this.picdir = picdir;
	}

	public Integer getShowpic() {
		return showpic;
	}

	public void setShowpic(Integer showpic) {
		this.showpic = showpic;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFromurl() {
		return fromurl;
	}

	public void setFromurl(String fromurl) {
		this.fromurl = fromurl;
	}


	public String getTitle_like() {
		return title_like;
	}

	public void setTitle_like(String title_like) {
		this.title_like = title_like;
	}

	public String getInputcode_like() {
		return inputcode_like;
	}

	public void setInputcode_like(String inputcode_like) {
		this.inputcode_like = inputcode_like;
	}

	public String getWxAccount() {
		return wxAccount;
	}

	public void setWxAccount(String wxAccount) {
		this.wxAccount = wxAccount;
	}

	@Override
	public String toString() {
		return "MsgNews [wxAccount=" + wxAccount + ", title=" + title
				+ ", title_like=" + title_like + ", author=" + author
				+ ", brief=" + brief + ", description=" + description
				+ ", picpath=" + picpath + ", picdir=" + picdir + ", showpic="
				+ showpic + ", url=" + url + ", fromurl=" + fromurl
				+ ", msgtype=" + msgtype + ", inputcode=" + inputcode
				+ ", inputcode_like=" + inputcode_like + ", rule=" + rule
				+ ", enable=" + enable + ", readcount=" + readcount
				+ ", favourcount=" + favourcount + "]";
	}


}