package com.cms.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/** 
 * 非法访问资源过滤器.
 * 
 * <p>在web.xml中的配置如下：</p>
 * <pre style="color:green;background:#ccc;">
 * &lt;filter&gt;
 * 	&lt;filter-name&gt;accessDeniedFilter&lt;/filter-name&gt;
 * 	&lt;filter-class&gt;fuc.common.filter.AccessDeniedFilter&lt;/filter-class&gt;
 * 	&lt;init-param&gt;
 * 		&lt;param-name&gt;redirect&lt;/param-name&gt;
 * 		&lt;param-value&gt;/app/html/error_page_404.html&lt;/param-value&gt;
 * 	&lt;/init-param&gt;
 * &lt;/filter&gt;
 * &lt;filter-mapping&gt;
 * 	&lt;filter-name&gt;accessDeniedFilter&lt;/filter-name&gt;
 * 	&lt;url-pattern&gt;/install/data/*&lt;/url-pattern&gt;
 * &lt;/filter-mapping&gt;
 * &lt;filter-mapping&gt;
 * 	&lt;filter-name&gt;accessDeniedFilter&lt;/filter-name&gt;
 * 	&lt;url-pattern&gt;*.properties&lt;/url-pattern&gt;
 * &lt;/filter-mapping&gt;</pre>
 */
public class AccessDeniedFilter implements Filter {
	private String redirect;
	
	/** 初始化. */
	public void init(FilterConfig filterConfig) throws ServletException {
		redirect = filterConfig.getInitParameter("redirect");
	}
	
	/** 过滤. */
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		response.sendRedirect(redirect);
	}
	
	/** 销毁 .*/
	public void destroy() {}
}