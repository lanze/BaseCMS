package com.cms.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cms.entity.Order;
import com.cms.service.MyBaseService;
import com.cms.support.Result;

@Controller
@RequestMapping(value = "api")
public class OrderApiController {
	@Autowired
	private MyBaseService myBaseService;

	// 预约
	@ResponseBody
	@RequestMapping(value = "/order/add")
	public Object add(Order order) {
		try {
			myBaseService.saveOrUpdate(order);
			myBaseService.updateByX("update Bus set status=1 where id=? ", order.getBusId());
			return new Result();
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "系统异常,请稍后再试");
		}
	}

	// 订单状态更新
	@ResponseBody
	@RequestMapping(value = "/order/updateStatus")
	public Object updateStatus(Long id,Long busId, Integer status) {
		try {
			myBaseService.updateByX("update Order set status=? where id=?",
					status, id);
			myBaseService.updateByX("update Bus set status=0 where id=? ", busId);
			return new Result();
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "系统异常,请稍候再试");
		}
	}

	// 我的订单列表
	@ResponseBody
	@RequestMapping(value = "/order/list")
	public Object list(String userName, Integer pageSize, Integer pageNum) {
		try {
			Order order = new Order();
			order.setUserId(userName);
			List<Order> list = myBaseService.listByExample(order, pageNum,
					pageSize);
			return new Result(true, list);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "系统异常,请稍后再试");
		}
	}

	// 订单详情
	@ResponseBody
	@RequestMapping(value = "/order/detail")
	public Object detail(Long id) {
		try {
			Order order = (Order) myBaseService.get(Order.class, id);
			return new Result(true, order);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "系统异常,请稍后再试");
		}
	}
}
