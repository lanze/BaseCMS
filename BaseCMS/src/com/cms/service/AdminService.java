package com.cms.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.cms.entity.Admin;

public interface AdminService extends UserDetailsService {
	public void recordLogin(String username, String remoteAddr);
	public List<Admin> listUsers();
	public boolean createUser(Admin user);
	public boolean updateUser(Admin user);
	public List<Admin> listDateUsers(String userName,String realName, Integer from, Integer to);
	public Long getCount(String authority,String realName);
	public void removeById(String username);
	public Admin loadUser(String userName);
}
