<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp" %>
<link
	href="<c:url value="/js/umeditor/themes/default/css/umeditor.css"/>"
	type="text/css" rel="stylesheet">
<script type="text/javascript">
	window.UMEDITOR_HOME_URL = "js/umeditor/";
</script>

<script type="text/javascript" charset="utf-8"
	src="<c:url value="/js/umeditor/umeditor.config.js"/>"></script>
<script type="text/javascript" charset="utf-8"
	src="<c:url value="/js/umeditor/umeditor.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/umeditor/lang/zh-cn/zh-cn.js"/>"></script>
<style>
	.table-bordered {
		text-align:left;
	}
	.table-bordered>tbody tr:nth-child(odd) {
		background:#fff;
	}
	.table-bordered>tbody tr td {
		padding-left: 40px;
		line-height: 30px;
	}
</style>
</head>
<body>
<%@ include file="/WEB-INF/view/common.jsp"%>
<div class="form-container">
		<div>
			<span class="title">公共服务</span>
			<img src="<c:url value="/images/jiantou.png" />" />
			<span class="sec-title">公告管理</span>
			<img src="<c:url value="/images/jiantou.png" />" />
			<span class="sec-title">增加公告</span>
		</div>
		<hr />
	</div>
	<div id="main" style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form action="notice/add" name="addNoticeForm" id="addNoticeForm"
				method="post" enctype="multipart/form-data" class="form-inline form-index">
				<div style="margin: 15px;">
					<font color="red">*</font> 标　　题: <input name="title" id="title"
						style="width: 300px" class="form-control"/>
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> 状　　态: <select name=status id="status" class="form-control">
						<c:forEach items="${STATEMAP }" var="map">
							<option value="${map.key}">${map.value}</option>
						</c:forEach>
					</select>
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> 公告类型: 	
					    <html:select cssClass="form-control" id="type" name="type" collection="noticeType" selectValue="">
										<option value="">-请选择-</option>	
						</html:select>
				</div>
				<div style="margin: 15px;">
					<font color="red">*</font> 公告内容:
					<div id="editor" style="width: 1000px; height: 240px;" ></div>
				</div>
				<div style="margin: 15px;">
					<input type="button" onclick="addNotice();" value="保存" class="btn btn-default"> <input
						type="button" value="取消" onclick="goBack('/notice');" class="btn btn-default">
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
	function strlen(str){
	    var len = 0;
	    for (var i=0; i<str.length; i++) { 
	    var c = str.charCodeAt(i); 
	    if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) { 
	      len++; 
	    } else { 
	      len+=2; 
	    }
	    } 
	    return len;
	}
	//实例化编辑器
	var um = UM.getEditor('editor');
		function goBack(url) {
			location.href = "notice";
		}
		function addNotice() {
			if ($('#title').val() == "") {
				$("#alert-content").html("请输入标题！");
				$("#alert-modal").modal("show");
				$('#title').focus();
				return false;
			}
			if ($('#status').val() == "") {
				$("#alert-content").html("请选择状态！");
				$("#alert-modal").modal("show");
				$('#status').focus();
				return false;
			}
			if ($('#type').val() == "") {
				$("#alert-content").html("请选择类型！");
				$("#alert-modal").modal("show");
				$('#type').focus();
				return false;
			}
			if (UM.getEditor('editor').getContent() == "") {
				$("#alert-content").html("请输入内容！");
				$("#alert-modal").modal("show");
				return false;
			}else if(strlen($.trim(UM.getEditor('editor').getContent()))>4000)
					{
					$("#alert-content").html("内容长度应少于4000个字符或2000个中文");
					$("#alert-modal").modal("show");
					return false;
					}
			$.ajax({
				url : "notice/add",
				data : {
					title:$("#title").val(),
					type:$("#type").val(),
					status:$("#status").val(),
					contents:UM.getEditor('editor').getContent()
				},
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						$("#alert-content").html("操作成功");
						$("#alert-ok").bind("click",function(){
							window.location.href = "notice";
						});
						$("#alert-modal").modal("show");
					} else {
						$("#alert-content").html("操作失败：内容超过4000字符（或2000个中文）");
						$("#alert-modal").modal("show");
					}
				},
				/* error : function(result) {
					$.messager.alert("提示", "操作失败：" + result.message);
				} */
				error:function(){
					$("#alert-content").html("您没有该权限,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		}
	</script>
</body>
</html>