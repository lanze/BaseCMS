<%@ taglib prefix="myfn" uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form:form commandName="user" id="form">
				<table class="table table-bordered">
					<tr>
						<td>操作员账号:</td>
						<td><form:input path="username" id="userName"
								class="form-control"
								onkeyup="value=value.replace(/[^\w\.\/]/ig,'')" /></td>
					</tr>
					<tr>
						<td>密码:</td>
						<td><form:password path="password" id="pwd"
								class="form-control" /></td>
					</tr>
					<tr>
						<td>重复密码:</td>
						<td><form:password path="passwordrep" id="re_pwd"
								class="form-control" /></td>
					</tr>
					<tr>
						<td>姓名:</td>
						<td><form:input path="realname" class="form-control" /></td>
					</tr>
					<tr>
						<td>部门:</td>
						<td><form:input path="department" class="form-control" /></td>
					</tr>
				</table>

				<p>
					权限: <span id="authError" style="color: red"></span>
				</p>
				<p>
					<c:forEach items="${roles}" var="role">
						<div style="float: left;">
							<input name="authorities" type="checkbox" value="${role.id}" />
							${role.authority}
						</div>
					</c:forEach>
				</p>
				<div style="clear: both"></div>
				<hr />
				<input type="button" value="保存" onclick="doSubmit();"
					class="btn btn-default">
			</form:form>
		</div>
	</div>
	<script type="text/javascript">
		function doSubmit() {
			var userName = $("#userName").val();
			var password = $("#pwd").val();
			var re_pass = $("#re_pwd").val();
			if (userName == '') {
				layer.alert("请输入登录账号");
				return;
			}
			if (password == '') {
				layer.alert("请输入密码");
				return;
			}
			if (re_pass == '') {
				layer.alert("请再次输入密码");
				return;
			}
			if (password != re_pass) {
				layer.alert("两次密码不一致");
				return;
			}
			if (!validateStr(password)) {
				layer.alert(" 密码必须包含数字、字母,长度为大于6位");
				return;
			}
			if ($(':checked').length == 0) {
				layer.alert("请选择权限！");
				return;
			}
			var data = $("#form").serialize();
			console.log(data);
			$.ajax({
				url : "add",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); //获取窗口索引
									parent.layer.close(index);
								});
						parent.reload();
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('系统异常');
				}
			});
		}
		function validateStr(pass) {
			var patrn = /^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){6,19}$/;
			if (!patrn.exec(pass))
				return false;
			return true;
		}
	</script>
</body>
</html>