<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<html:listPage ajaxUrl="account"
		columns="id,account,name,appid,appsecret,url,token,msgcount,createDate,modifyDate"
		titles="编码,公众号,公众号名称,appid,密钥,url地址,token,自动回复消息数,创建时间,修改时间"
		queryIds="account-input,name_like-input,status-select|00_正常/01_关闭,createDate_start|createDate_end-date,modifyDate_start|modifyDate_end-date"
		queryTitles="公众号,公众号名称,状态,创建时间,修改时间">
<%-- 		<div class="form-group">
			<label class="control-label">状态</label>
			<html:select cssClass="form-control" collection="articleStatus"
				selectValue="${article.status }" name="status" id="status">
				<option value="">全部</option>
			</html:select>
		</div> --%>
	</html:listPage>
</body>
</html>