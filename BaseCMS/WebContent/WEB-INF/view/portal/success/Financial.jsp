<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>四川省金融IC公共服务平台</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/success.css"/>" />
</head>

<body>
	<div class="outDiv">
		<div class="container con-container">	
			<img src="<c:url value="/images/portal/proCase/TSM1.png"/>" class="img"/>
			<div class="content-one">随着彩票电子化网络化的发展、购彩的形式丰富多样、必然需要便捷安全的支付系统配合,线下站点的购彩模式已然不能满足市场需求。</div>
			<div class="content-one">卡卡付推出认证支付，在时代的驱动下与<span style="color:#000;"><b>四川福彩</b></span>合作，手机购彩神器“蜀彩宝”正式上线，用户无需开通网银，完成银行卡信息认证后即可实现无卡轻松支付，手机购彩更便捷。</div>
			
		</div>
	</div>
</body>
</html>
