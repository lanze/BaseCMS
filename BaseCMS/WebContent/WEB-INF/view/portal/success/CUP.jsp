<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>银联手机支付平台</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/success.css"/>" />
<style>
	
</style>
</head>

<body>
	<div class="outDiv">
		<div class="container con-container">	
			<img src="<c:url value="/images/portal/proCase/phonePay.png"/>" class="img"/>
			<div class="content-one">卡卡付协同<span style="color:#000;"><b>当乐网</b></span>，将NFC支付成功应用于当乐网集成所有游戏的手机客户端上。用户只需拥有一部具备NFC功能的手机、一张带Quick Pass（闪付）功能的芯片银行卡，无需开通网银、无需注册，一挥之间，便可在手机上完成游戏币充值。</div>
			<div class="content-one">卡卡付支付服务覆盖手游、页游、端游及H5游戏，提供完整的游戏支付整体解决方案。</div>
		</div>
	</div>
</body>
</html>
