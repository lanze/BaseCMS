<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>微信支付-主页</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<style>
.section {
	height:100% !important;
	width:100% !important;
}
.section>.tableCell {
	height: 100% !important;
}
#fullPage-nav li .active span {
    background: #14B658;
}
#fullPage-nav span {
	width: 14px;
    height: 14px;
    border: 1px solid #14B658;
}
#fullPage-nav li {
	margin: 10px;
}
#fullPage-nav.left {
    left: 40px;
}
</style>
</head>
<body>
	<div class="navbar-pay">		
		<div class="nav_width" style="background:none;">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo"/></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" class="active">主页</a></li>
					<li><a href="<c:url value="/portal/product"/>" >产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>" >成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>" >安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>" >技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>" >关于我们</a></li>
				</ul> 
			</div>
		</div>
	</div>
	
	<div class="section section1">
		<!-- <img src="<c:url value="/images/portal/one/oneBg.jpg"/>" class="bg"/>  onload="imgAutoBoat(this);"-->
		<img src="<c:url value="/images/portal/one/boat.png"/>" class="boat" onload="imgAuto(this);" />
		<div class="container">
			<div>
				<img src="<c:url value="/images/portal/one/i.png"/>" class="i" onload="imgAuto(this);" />
				<img src="<c:url value="/images/portal/one/kkf2016.png"/>" class="kkf2016" onload="imgAuto(this);"/><br>
				<img src="<c:url value="/images/portal/one/yang.png"/>" class="yang" onload="imgAuto(this);"/>
			</div>
		</div>
		<img src="<c:url value="/images/portal/one/out.png"/>" class="bottom" onload="imgAuto(this);"/>
	</div>
	<div class="section section2">
		<!--<img src="<c:url value="/images/portal/two/twoBg.jpg"/>" class="bg" /> -->
		<div class="container">
			<div>
				<img src="<c:url value="/images/portal/two/kkf.png"/>" class="kkf bottom" onload="imgAuto(this);"/><br>
				<img src="<c:url value="/images/portal/two/nfc.png"/>" class="nfc bottom" onload="imgAuto(this);"/><br>
				<a href="<c:url value="/zpos"/>" target="_blank"><img src="<c:url value="/images/portal/two/enter.png"/>" class="enter bottom" style="display:none;" onload="imgAuto(this);"/></a><br>
				<img src="<c:url value="/images/portal/two/QRcode.png"/>" class="QRcode bottom" style="display:none;" onload="imgAuto(this);"/><br>
			</div>			
			<img src="<c:url value="/images/portal/two/rocket.png"/>" class="rocket" onload="imgAuto(this);" />
			<img src="<c:url value="/images/portal/two/zpos.png"/>" class="zpos" onload="imgAuto(this);"/>
			<img src="<c:url value="/images/portal/two/iphone6.png"/>" class="iphone6" onload="imgAuto(this);" />
		</div>		
	</div>
	<div class="section section3">
		<!--<img src="<c:url value="/images/portal/thr/thrBg.jpg"/>" class="bg"/>-->
		<div class="container">
			<div class="top">
				<img src="<c:url value="/images/portal/thr/nfc.png"/>" class="nfc" onload="imgAuto(this);"/><br>
				<img src="<c:url value="/images/portal/thr/ka.png"/>" class="ka" onload="imgAuto(this);"/>
			</div>
			
		</div>
		<img src="<c:url value="/images/portal/thr/phone.png"/>" class="phone" onload="imgAuto(this);"/>
		<img src="<c:url value="/images/portal/thr/bankCard.png"/>" class="bankCard" onload="imgAuto(this);"/>
	</div>
	<div class="section section4">
		<!--<img src="<c:url value="/images/portal/four/fourBg.jpg"/>" class="bg"/>-->
		<div class="container">
			<div class="left">
				<img src="<c:url value="/images/portal/four/pay.png"/>" class="pay" onload="imgAuto(this);" /><br>
				<img src="<c:url value="/images/portal/four/17.png"/>" class="seven-1" onload="imgAuto(this);"/>
			</div>
			<div class="right">
				<img src="<c:url value="/images/portal/four/payWei.png"/>" class="pay" onload="imgAuto(this);"/><br>
				<img src="<c:url value="/images/portal/four/72.png"/>" class="seven-1" onload="imgAuto(this);"/>
			</div>
			<div class="img">
				<img src="<c:url value="/images/portal/four/jiaotong_1.png"/>" class="jiaotong_1" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/zhong_2.png"/>" class="zhong_2"  onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/china_2.png"/>" class="china_2" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/nonhang_1.png"/>" class="nonhang_1" onload="imgAuto(this);" />
				<img src="<c:url value="/images/portal/four/CBC_1.png"/>" class="CBC_1" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/huaxia_2.png"/>" class="huaxia_2" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/bohai_2.png"/>" class="bohai_2" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/huaxia_1.png"/>" class="huaxia_1" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/jianshe.png"/>" class="jianshe1" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/jiaotong_2.png"/>" class="jiaotong_2" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/bohai_1.png"/>" class="bohai_1" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/nonhang_2.png"/>" class="nonhang_2" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/china_1.png"/>" class="china_1" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/CBC_2.png"/>" class="CBC_2" onload="imgAuto(this);"/>
				<img src="<c:url value="/images/portal/four/zhong_1.png"/>" class="zhong_1" onload="imgAuto(this);"/>
			</div>
		</div>
	</div>
	<div class="section section5">
		<!--<img src="<c:url value="/images/portal/five/fiveBg.jpg"/>" class="bg"/>-->
		<div class="container">
			<div>
				<div class="title">合作银行</div>
				<div class="bank">
					<img src="<c:url value="/images/portal/five/bank/yinlian.png"/>" title="银联" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/zhongguo.png"/>" title="中国银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/ICBC.png"/>" title="工商银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/jianshe.png"/>" title="建设银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/jiaotong.png"/>" title="交通银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/youchu.png"/>" title="邮政储蓄" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/zhaoshang.png"/>" title="招商银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/zhongxin.png"/>" title="中信银行" onload="imgAuto(this);"/><br>
					<img src="<c:url value="/images/portal/five/bank/minsheng.png"/>" title="民生银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/guangda.png"/>" title="光大银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/pufa.png"/>" title="浦发银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/guangfa.png"/>" title="广发银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/nonhang.png"/>" title="农业银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/xingye.png"/>" title="兴业银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/huaxia.png"/>" title="华夏银行" onload="imgAuto(this);"/>
					<img src="<c:url value="/images/portal/five/bank/bohai.png"/>" title="渤海银行" onload="imgAuto(this);"/>
				</div>
				
				<div class="title" style="margin-top: 25px;">合作商户</div>
				<div class="shanghu">
					<img src="<c:url value="/images/portal/five/qq.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/dangle.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/niannianka.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/lianbao.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/shucaibao.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/renbao.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/hangtian.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/xinfalian.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/ziyuanzhe.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/beecloud.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/p++.png"/>" onload="imgAuto(this);" />
					<img src="<c:url value="/images/portal/five/aibeiyun.png"/>" onload="imgAuto(this);" />
				</div>
			</div>
		</div>		
	</div>
	
	<div class="footer-one navbar-fixed-bottom">
		<div class="container">
			<ul>
				<c:forEach items="${list}" var="article" varStatus="idxStatus">
					<c:if test="${idxStatus.index == 0}">
						<li style="color:#FFDD5D;"><a href="portal/article/view?id=${article.id}"style="color:#FFDD5D;">${article.title}</a></li>
					</c:if>
					
					<c:if test="${idxStatus.index == 1}">
						<li style="color:#FFDD5D;">
							<a href="portal/article/view?id=${article.id}" >${article.title}</a>
						</li>
					</c:if>
					
					<c:if test="${idxStatus.index == 2}">
						<li style="color:#FFDD5D;">
							<a href="portal/article/view?id=${article.id}" >${article.title}</a>
							<a href="portal/article/more?pageNo=1" style="float: right;">更多</a>
						</li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</div>
	
	<div class="footer navbar-fixed-bottom" style="background-color: #1D394B;display:none;">
		<div class="Copyright" style="color:#5B7C87;">Copyright 版权所有：成都卡卡付科技有限公司 All Right Reserve 蜀ICP备16003612号</div>
	</div> 
	
	<!--请将以下这段代码放到网页中您想要显示固定图标的位置-->  
	<div id="FloatDIV" style="position: absolute;top: 40%; right:40px;border-right: activeborder ; border-top: activeborder ; border-left: activeborder ; border-bottom: activeborder ; z-index:9999">  
	    <a href="tencent://message/?Menu=yes&amp;amp;uin=800138787&amp;amp;Service=58&amp;amp;SigT=A7F6FEA02730C988B9AA700B1A25250A10A54CE3273AE5494FFF2D6200FF4316B549124DBA1FC2FE5A92AA976B2FDA33935060C4316B525D50AD6333AB3C632137665336B9A6A1CAC695C6AD19E7097A64DB7072E6E52BE684518F4D915D16978C8A5D570BDB785B47BE528968DA80EAF6C8E0DD7ADDB89C&amp;amp;SigU=30E5D5233A443AB2C7B3CB0D090F7EDDD0F442F533247CAB05D9C85B857A4557A21B0B4A4FE8ED637E9EC46227AF08435A17CE9703349B414AAB577F2DEE061816CAB91D5B4EEA1A">
	    	<img src="<%=path%>/images/portal/one/qq.png" /> 
	    </a>
	</div> 
	
<script>
/*
	function imgAuto() {
		 for(var i=0;i<$('.section img').length;i++) {
			 $('.section img')[i].onload = function(obj){
				$(this).height(($(this).height()) * 0.6);
			 };
	    }
	}
	
	
	
	
	if(!window.name){
        window.name = 'test';
        window.location.reload();
	}
	if($(window).height() <800) {
		for(var i=0;i<$('.section img').length;i++) {
			 $('.section img')[i].onload = function(obj){
				$(this).height(($(this).height()) * 0.6);
			 };
	    }
		
	}*/
	
	
	
/*
	var heightWin = $(window).height();
	function imgAuto(objImg) {
		if(heightWin <800) {
			var heightNew = ($(objImg).height()) * 0.6;
			$(objImg).height(heightNew);
		}
	}*/


 $(function(){ 
	 $('.section1').find('.boat').delay(500).animate({
		left: '22%'
	 }, 2000, 'easeOutExpo');
	 $('.section1').find('.container .kkf2016').delay(1500).fadeIn(500).animate({
		top: '0'
	 }, 1500, 'easeOutExpo');
	 $('.section1').find('.container .i').delay(1500).fadeIn(2000);
	 $('.section1').find('.container .yang').delay(1500).fadeIn(500).animate({
		right: '0'
	 }, 1500, 'easeOutExpo');
	 
	 $.fn.fullpage({
		anchors: ['page1', 'page2', 'page3', 'page4', 'page5', 'page6'],
		navigation: true,
		navigationPosition: "left",
		navigationColor: "#16B55A",
		afterLoad: function(anchorLink, index){
			if(index == 1){
				$('.footer-one').css({"display":"block"});
				$('.section1').find('.boat').delay(500).animate({
					left: '22%'
				 }, 2000, 'easeOutExpo');
				 $('.section1').find('.container .kkf2016').delay(1500).fadeIn(500).animate({
					top: '0'
				 }, 1500, 'easeOutExpo');
				 $('.section1').find('.container .i').delay(1500).fadeIn(2000);
				 $('.section1').find('.container .yang').delay(1500).fadeIn(500).animate({
					right: '0'
				 }, 1500, 'easeOutExpo');
				 $('.nav_width a>.logo').attr('src','<c:url value="/images/portal/one/ikkpay_1.png"/>');
				 $('.navbar-pay ul>li>a').css({"color":"#333","opacity":"1"});
				 $('.navbar-pay ul>li>.active').css({"color":"#21b74e","border-bottom":"4px solid #21b74e","opacity":"1"});
				 $(".navbar-pay ul>li>a").hover( function(event){
				    $(this).css({"color":"#21b74e","border-bottom":"4px solid #21b74e"});    
				}, function(event){
				    $(this).css({"color":"#333","border-bottom":"none","opacity":"1"});
				} );
				 $('.navbar-pay .top div>a, .navbar-pay .top>div>span').css({"color":"#9E9D98","opacity":"1"});
				 $('.navbar-pay .top div>.phone').css({"color":"#21b74e","opacity":"1"});
				 $('#fullPage-nav li .active span').css({"background": "#14B658"});
				 $('#fullPage-nav span').css({"border":"1px solid #14B658"});
				 $('#FloatDIV').css({"top":($(window).height())*0.4});   //QQ
				 $('.footer').hide();
			}else {
				$('.footer-one').css({"display":"none"});
				$('.nav_width a>.logo').attr('src','<c:url value="/images/portal/one/ikkpay_2.png"/>');
				$('.navbar-pay ul>li>a').css({"color":"#fff","opacity":"0.7"});
				$('.navbar-pay ul>li>.active').css({"border-bottom":"4px solid #fff","opacity":"1"});
				$('#fullPage-nav span').css({"border":"1px solid #fff"});    //导航条
				$(".navbar-pay ul>li>a").hover( function(event){
				    $(this).css({"color":"#fff","opacity":"1","border-bottom":"4px solid #fff"});    
				}, function(event){
				    $(this).css({"color":"#fff","opacity":"0.7","border-bottom":"none"});
				} );
				$('.navbar-pay .top div>a').css({"color":"#fff","opacity":"0.5"});
				$('.navbar-pay .top div>span').css({"color":"#fff","opacity":"0.7"});
				$('.footer').hide();
				if(index == 2){
					$('.section2').find('.container .enter').fadeIn(2000);
					$('.section2').find('.container .QRcode').fadeIn(2000);
					$('.section2').find('.container .kkf').delay(1500).fadeIn(500).animate({
						left: '0'
					}, 1000, 'easeOutExpo');
					$('.section2').find('.container .nfc').delay(2000).fadeIn(500).animate({
						left: '0'
					}, 1000, 'easeOutExpo');
					
					if($(window).height()<800) {
						$('.section2').find('.container>.rocket').delay(500).animate({
							left: '606px', bottom: '120px'
						}, 2000, 'easeOutExpo');
					}else {
						$('.section2').find('.container>.rocket').delay(500).animate({
							left: '620px', bottom: '200px'
						}, 2000, 'easeOutExpo');
					}
					$('.section2').find('.container>.zpos').fadeIn(2000);
					$('.section2').find('.container>.iphone6').fadeIn(2000);
					$('#FloatDIV').css({"top":$(window).height()+($(window).height())*0.4});   //QQ
					$('#fullPage-nav li a[class=active] span').css({"background": "#fff"});
					$('#fullPage-nav li a[class!=active] span').css({"background": "none"});
				}
				if(index == 3){
					$('.section3').find('.bankCard').delay(500).fadeIn(500).animate({
						left: '42%'
					}, 3000, 'easeOutExpo');
					$('.section3').find('.container .nfc').delay(1500).fadeIn(500).animate({
						right: '0'
					}, 1500, 'easeOutExpo');
					$('.section3').find('.container .ka').delay(2500).fadeIn(500).animate({
						right: '0'
					}, 1500, 'easeOutExpo');
					$('#FloatDIV').css({"top":($(window).height())*2+($(window).height())*0.4});   //QQ
					$('#fullPage-nav li a[class=active] span').css({"background": "#fff"});
					$('#fullPage-nav li a[class!=active] span').css({"background": "none"});
				}
				if(index == 4){
					//$('.section4').find('.container>.img>img').fadeIn(2000);
					$('.section4').find('.container>.img>img[class*=2]').delay(1000).fadeIn(1000);
					$('.section4').find('.container>.img>img[class*=1]').delay(1000).fadeIn(4000);
					$('.section4').find('.container .left .pay').delay(2000).fadeIn(500).animate({
						left: '20px'
					}, 1500, 'easeOutExpo');
					$('.section4').find('.container .left .seven-1').delay(3500).fadeIn(500).animate({
						left: '20px'
					}, 1500, 'easeOutExpo');
					$('.section4').find('.container .right .pay').delay(2000).fadeIn(500).animate({
						left: '20px'
					}, 1500, 'easeOutExpo');
					$('.section4').find('.container .right .seven-1').delay(3500).fadeIn(500).animate({
						left: '20px'
					}, 1500, 'easeOutExpo');	
					$('#FloatDIV').css({"top":($(window).height())*3+($(window).height())*0.4});   //QQ
					$('#fullPage-nav li a[class=active] span').css({"background": "#fff"});
					$('#fullPage-nav li a[class!=active] span').css({"background": "none"});
				}
				if(index == 5){
					$('#FloatDIV').css({"top":($(window).height())*4+($(window).height())*0.4});   //QQ
					$('#fullPage-nav li a[class=active] span').css({"background": "#fff"});
					$('#fullPage-nav li a[class!=active] span').css({"background": "none"});
					$('.footer').show();
				}
			}
		},
		onLeave: function(index, direction){
			if(index == '1'){				
				setTimeout(function() {
					$('.section1').find('.boat').css({"left":"18%"});
					 $('.section1').find('.container .kkf2016').css({"right":"0","display":"none","top":"-50px"});
					 $('.section1').find('.container .i').css({"right":"124px","display":"none"});
					 $('.section1').find('.container .yang').css({"right":"-50px","display":"none","top":"10"});
				},2000);
			}else{
				if(index == '2'){
					setTimeout(function() {
						$('.section2').find('.container .kkf').css({"display":"none","left":"100px"});
						$('.section2').find('.container .nfc').css({"display":"none","left":"100px" });
						$('.section2').find('.container .enter').css({"display":"none"});
						$('.section2').find('.container .QRcode').css({"display":"none"});
						$('.section2').find('.container>.rocket').css({"bottom":"0","left":"321px"});
						if($(window).height()<800) {
							$('.section2').find('.container>.rocket').css({"bottom":"-50","left":"366px"});
						}
						$('.section2').find('.container>.zpos').css({"display":"none"});
						$('.section2').find('.container>.iphone6').css({"display":"none"});
					},2000);
				}
				if(index == '3'){
					setTimeout(function() {
						$('.section3').find('.bankCard').css({"display":"none","left":"52%"});
						$('.section3').find('.container .nfc').css({"display":"none","right":"-110px"});
						$('.section3').find('.container .ka').css({"display":"none","right":"-110px"});
					},2000);
				}
				if(index == '4'){
					setTimeout(function() {
						$('.section4').find('.container>.img>img').css({"display":"none"});
						$('.section4').find('.container .left .pay').css({"display":"none","left":"-60px"});
						$('.section4').find('.container .left .seven-1').css({"display":"none","left":"-60px"});
						$('.section4').find('.container .right .pay').css({"display":"none","left":"-60px"});
						$('.section4').find('.container .right .seven-1').css({"display":"none","left":"-60px"});
					},2000);
					
				}
				
			}
		}
	});
}); 
 /*
	$(window).on('scroll', function() {
		var scrollTop = $(document).scrollTop(); //滚动条距离顶端的高度
		if (scrollTop > 0) {
			$('.navbar-pay .container').css({
				"padding-top" : "8px"
			});
			$('.navbar-pay .nav-pay>li>a').css({
				"padding-bottom" : "18px"
			});
		} else {
			$('.navbar-pay .container').css({
				"padding-top" : "18px"
			});
			$('.navbar-pay .nav-pay>li>a').css({
				"padding-bottom" : "26px"
			});
		}  
	});*/             

	$('.nav-pay li').on("click", function() {
		if ($(this).hasClass("active") == false) {
			$(this).addClass("active").siblings().removeClass("active");     
		}
	});
	
	
	
	/*
  var MarginLeft = 15;  //浮动层离浏览器右侧的距离  
  var MarginTop = 100;   //浮动层离浏览器顶部的距离  
  var Width = 92;            //浮动层宽度  
  var Heigth= 152;           //浮动层高度  
    
  //设置浮动层宽、高  
  function Set()  
  {  
      document.getElementById("FloatDIV").style.width = Width + 'px';  
      document.getElementById("FloatDIV").style.height = Heigth + 'px';  
  }  
    function ccc()
	{
		var offsetobj = $("#content_background").offset();
		alert(offsetobj.top);
		alert($("#FloatDIV").offset().top);
	}
  //实时设置浮动层的位置  
  function Move()  
  {  
        var b_top =  window.pageYOffset    
                || document.documentElement.scrollTop    
                || document.body.scrollTop    
                || 0;  
        var b_width= document.body.clientWidth; 
		var top = b_top + MarginTop ;
		if(top<220)
		{
		 top=380;
		}
      document.getElementById("FloatDIV").style.top = top + 'px';  
      document.getElementById("FloatDIV").style.left = b_width - Width - MarginLeft + 'px';  
      setTimeout("Move();",100);  
  }  
    
  Set();  
  Move();  
  */
  
    
</script> 	
	
<!-- WPA Button Begin -->
<script charset="utf-8" type="text/javascript" src="wpa.b.qq.com/cgi/wpa.php?key=XzgwMDEzODc4N18zMTQ1NDJfODAwMTM4Nzg3Xw"></script>
<!-- WPA Button END -->
<!-- WPA Button Begin -->
<script charset="utf-8" type="text/javascript" src="wpa.b.qq.com/cgi/wpa.php?key=XzgwMDEzODc4N18zMTQ1NTFfODAwMTM4Nzg3Xw"></script>
<!-- WPA Button End -->

</body>
</html>
