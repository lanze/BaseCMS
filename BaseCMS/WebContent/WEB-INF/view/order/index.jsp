<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp"%>
</head>
<body>
	<%@ include file="/WEB-INF/view/common.jsp"%>
	<div id="main" style="margin-left: auto; margin-right: auto;">

		<div class="form-container" id="search">
			<div>
				<span class="title">业务管理</span> <img
					src="<c:url value="/images/jiantou.png" />" /> <span
					class="sec-title">订单管理</span>
			</div>
			<hr />
			<form class="form-inline form-index" action="#" method="post"
				id="form">
				<div class="form-group">
					<label class="control-label">出发地点</label> <input name="fromAddr"
						id="fromAddr" type="text"  class="form-control" />
				</div>
				<div class="form-group">
					<label class="control-label">目的地点</label> <input name="toAddr"
						id="toAddr" type="text"  class="form-control" />
				</div>
				<div class="form-group">
					<label class="control-label">状态</label> <select name="status"
						id="status" class="form-control">
						<option value="">--全部--</option>
						<option value="0">待审核</option>
						<option value="1">审核通过</option>
						<option value="1">审核拒绝</option>
					</select>
				</div>

				<div class="form-group button-group" style="width: 100%;">

					<html:auth res="/order/list">
						<input onclick="queryPage();" type="button" id="search" value="查询"
							class="btn btn-default">
					</html:auth>
					<input onClick="resetClear();" type="reset" id="reset" value="重置"
						class="btn btn-default">
				</div>
			</form>
		</div>
		<div class="table-container" id="content"></div>
		<div id="over" class="over">
			<div id="layout" class="layout">
				<img src="<c:url value="/images/loaderc.gif"/>" />
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			queryPage();
		});
		$.fn.datebox.defaults.formatter = function(date) {
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			var d = date.getDate();
			return y + '-' + m + '-' + d;
		};
		function queryPage(pageNo, pageSize) {
			showLoading();
			var params = $("#form").serialize();
			$.ajax({
				url : "order/list",
				type : "post",
				data : params,
				cache : false,
				success : function(html) {
					$("#over").css("display", "none");
					$("#layout").css("display", "none");
					$('#content').replaceWith(html);
				},
				error : function() {
					$("#alert-content").html("系统异常,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		};
		function showLoading() {
			document.getElementById("over").style.display = "block";
			document.getElementById("layout").style.display = "block";
		}
		function audit(id,status) {
			$.ajax({
				url : "order/audit",
				type : "POST",
				data : {
					id : id,
					status:status
				},
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.flag) {
						$("#alert-content").html("审核成功");
						$("#alert-ok").bind("click", function() {
							window.location.href = "order";
						});
						$("#alert-modal").modal("show");
					} else {
						$("#alert-content").html(result.message);
						$("#alert-modal").modal("show");
					}
				},
				/* error : function() {
					$.messager.alert("提示", result.message);
				} */
				error : function() {
					$("#alert-content").html("您没有该权限,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		};
		//重置，清空datebox
		function resetClear() {
			$('#startDate').datebox('setValue', '');
			$('#endDate').datebox('setValue', '');
		}
	</script>
</body>
</html>